"""
All utils live here
"""
import pandas as pd


def convert_to_datetime(dataframe, column):
    """Converts a column of string into date"""
    try:
        dataframe[column] = pd.to_datetime(dataframe[column])
        dataframe.sort(column)
    except (AttributeError, TypeError):
        return
    except KeyError:
        print(f"The column {column} does not exist")
    return dataframe
