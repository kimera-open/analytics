"""
All the loaders live here
"""

import pandas as pd


def get_dataframe(file=None):
    """
    Loads the dataframe from the file
    :param file: file path
    :return:
    """
    try:
        df = pd.read_csv(r"{}".format(file.strip()))
    except Exception:
        print("Not being able to load the file")
        return
    return df
