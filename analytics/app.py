"""
Main application being loaded through console
"""
import argparse

import matplotlib.pyplot as plt

from analytics.loaders import get_dataframe
from analytics.utils import convert_to_datetime


def show_table(filepath):
    """
    Executes the program and gets the dataframe
    1. Converts a column of an episode into Datetime
    :param filepath: File location
    """
    df = get_dataframe(filepath)
    df = convert_to_datetime(df, "Trade Date")
    display_chart(df)


def display_chart(df):
    """
    Displays the dataframe in a char plot
    :param df: dataframe to show the episode
    """
    df.plot(df["Trade Date"], df['Trade Quantity'], kind='area', stacked=False)
    plt.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", dest='filepath', type=str, required=True)
    _args = parser.parse_args()
    show_table(_args.filepath)
