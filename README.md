# ESSENTIA ANALYTICS
- All the files are placed in this directory
- The requirements is placed inside the requirements folder

# Notes
- Python 3.6
- Venv

# Install Requirements
- You should have a venv in python 3.6 +
- Run `pip install -r requirements/common.txt`
- Inside the venv and in the root directory, run 
```shell script
export PYTHONPATH=`pwd`
```